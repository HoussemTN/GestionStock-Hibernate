/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author Houssem
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import bean.Piece;
import java.util.List;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author Houssem
 */
public class PieceService {
     public void create(Piece p){
    Session session;  
    session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.save(p);   
    session.getTransaction().commit();
     
   }
      public void update(Piece p){
    Session session;  
    session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.update(p);
    session.getTransaction().commit();
     
   }
    public void delete(Piece p){
    Session session;  
    session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.delete(p);
    session.getTransaction().commit();
     
   }
    public List<Piece> findAll(){
       Session session;  
       session = HibernateUtil.getSessionFactory().openSession();
       session.beginTransaction();   
       org.hibernate.Query query = session.createQuery("from Piece");
       List<Piece> Pieces= query.list();
       session.getTransaction().commit(); 
        return Pieces ;
          }
      
    
    
}


