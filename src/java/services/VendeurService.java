/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import bean.Vendeur;
import java.util.List;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author Houssem
 */
public class VendeurService {
     public void create(Vendeur p){
    Session session;  
    session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.save(p);   
    session.getTransaction().commit();
     
   }
      public void update(Vendeur p){
    Session session;  
    session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.update(p);
    session.getTransaction().commit();
     
   }
    public void delete(Vendeur p){
    Session session;  
    session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.delete(p);
    session.getTransaction().commit();
     
   }
    public List<Vendeur> findAll(){
       Session session;  
       session = HibernateUtil.getSessionFactory().openSession();
       session.beginTransaction();   
       org.hibernate.Query query = session.createQuery("from Vendeur");
       List<Vendeur> Vendeurs= query.list();
       session.getTransaction().commit(); 
        return Vendeurs ;
          }
      
    
    
}


