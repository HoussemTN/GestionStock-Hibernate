/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import bean.Fournisseur;
import java.util.List;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author Houssem
 */
public class FournisseurService {
     public void create(Fournisseur p){
    Session session;  
       session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    
    session.save(p);   
    session.getTransaction().commit();
     
   }
      public void update(Fournisseur p){
    Session session;  
       session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    
    session.update(p);
    session.getTransaction().commit();
     
   }
          public void delete(Fournisseur p){
    Session session;  
       session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();   
    session.delete(p);
    session.getTransaction().commit();
     
   }
    public List<Fournisseur> findAll(){
        Session session;  
        session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();   
       org.hibernate.Query query = session.createQuery("from Fournisseur");
           List<Fournisseur> Fournisseurs= query.list();
    session.getTransaction().commit(); 
    return Fournisseurs ;
          }
      
    
    
}

