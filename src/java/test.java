
import bean.Fournisseur;
import bean.Piece;
import bean.Vendeur;
import services.FournisseurService;
import services.PieceService;
import services.VendeurService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Houssem
 */
public class test {
    public static void main (String [] arg){
        // Create Fournisseur
        Fournisseur fournisseur= new Fournisseur("Asus","Tunis","Support-asus@asus.tn","71845000");
        FournisseurService fournisseurService = new FournisseurService();
        fournisseurService.create(fournisseur);
         // Create Piece
        Piece piece= new Piece("Asus","PC","XV550",10);
        PieceService pieceService = new PieceService();
        pieceService.create(piece);
         // Create vendeur
        Vendeur vendeur= new Vendeur("Souissi","Tunis","souissi-vendeur@asus.tn","28600000");
        VendeurService vendeurService = new VendeurService();
        vendeurService.create(vendeur);
    }
}
