/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author wamp
 */
@Entity
@Table(name = "fournisseur")
public class Fournisseur {
     @Id
    @GeneratedValue
    int id ; 
     
    String nom ; 
    
    String adresse ; 
   
    String mail ; 
   
    String tel ; 

    public Fournisseur(String nom, String adresse, String mail, String tel) {
        this.nom = nom;
        this.adresse = adresse;
        this.mail = mail;
        this.tel = tel;
    }

    public Fournisseur() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Fournisseur{" + "id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", mail=" + mail + ", tel=" + tel + '}';
    }
    
}

    
  