/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Houssem
 */
@Entity
@Table(name = "vendeur")
public class Vendeur {
     @Id
    @GeneratedValue
    int id ; 
    String nom ; 
    String adresse ; 
    String tel ; 
    String mail ; 

    public Vendeur(String nom, String adresse, String tel, String mail) {
        this.nom = nom;
        this.adresse = adresse;
        this.tel = tel;
        this.mail = mail;
    }

    public Vendeur() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "Vendeur{" + "id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", tel=" + tel + ", mail=" + mail + '}';
    }
   


    
}

    
  

