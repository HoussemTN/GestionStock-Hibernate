/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Houssem
 */
@Entity
@Table(name = "piece")
public class Piece {

     @Id
    @GeneratedValue
    int code ;
    String marque ; 
    String modele ; 
    String serie ; 
    int quantite ;

    public Piece(String marque, String modele, String serie, int quantite) {
        this.marque = marque;
        this.modele = modele;
        this.serie = serie;
        this.quantite = quantite;
    }

    public Piece() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public String toString() {
        return "Piece{" + "code=" + code + ", marque=" + marque + ", modele=" + modele + ", serie=" + serie + ", quantite=" + quantite + '}';
    }
    
   }
